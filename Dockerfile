FROM python:3.8 as base

# Install Poetry
ENV POETRY_VIRTUALENVS_CREATE=false
RUN pip install poetry


FROM base as dev

# Copy all tests, scripts, settings, etc. and do editable install
WORKDIR /app
COPY . .
RUN poetry install

CMD ["python", "-m", "testing_lab.main"]


FROM base as release

# Copy only code and install without dev dependencies
WORKDIR /app
COPY pyproject.toml poetry.lock ./
COPY src/ src/
RUN poetry install --no-dev

CMD ["python", "-m", "testing_lab.main"]
