import pytest
from testing_lab.pipeline import PipelineStateless


@pytest.fixture
def pipeline():
    return PipelineStateless()


def test_stateless_run(mocker, pipeline):
    mock_add = mocker.patch.object(pipeline, "add", return_value=2)
    mock_multiply = mocker.patch.object(pipeline, "multiply", return_value=3)

    result = pipeline.run(1)

    mock_add.assert_called_once_with(1)
    mock_multiply.assert_called_once_with(2)
    assert result == 3


def test_stateless_add(pipeline):
    assert pipeline.add(5) == 10


def test_stateless_multiply(pipeline):
    assert pipeline.multiply(5) == 15
