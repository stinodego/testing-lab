import pytest
from testing_lab.pipeline import PipelineStateful


@pytest.fixture
def pipeline():
    return PipelineStateful(5)


def test_stateful_init(pipeline):
    assert pipeline.x == 5


def test_stateful_run(mocker, pipeline):
    mock_add = mocker.patch.object(pipeline, "add")
    mock_multiply = mocker.patch.object(pipeline, "multiply")

    pipeline.run()

    assert mock_add.call_count == 1
    assert mock_multiply.call_count == 1
    assert pipeline.x == 5


def test_stateful_add(pipeline):
    pipeline.add()
    assert pipeline.x == 10


def test_stateful_multiply(pipeline):
    pipeline.multiply()
    assert pipeline.x == 15
