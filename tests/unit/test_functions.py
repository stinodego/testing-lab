import pytest
from testing_lab.functions import add, add_then_multiply, multiply


def test_add():
    assert add(5) == 10
    assert add(-5) == 0

    with pytest.raises(TypeError):
        add("5")


def test_multiply():
    assert multiply(5) == 15
    assert multiply(-5) == -15

    # with pytest.raises(TypeError):
    #     multiply("5")


def test_add_then_multiply(mocker):
    # Setup
    mock_add = mocker.patch("testing_lab.functions.add", return_value=2)
    mock_multiply = mocker.patch("testing_lab.functions.multiply", return_value=3)

    # Run
    result = add_then_multiply(1)

    # Check
    mock_add.assert_called_once_with(1)
    mock_multiply.assert_called_once_with(2)
    assert result == 3
