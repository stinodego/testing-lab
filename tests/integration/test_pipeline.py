from testing_lab.main import run_functions_pipeline


def test_pipeline(capsys):
    run_functions_pipeline()
    output = capsys.readouterr().out
    assert output == "30\n"
