from testing_lab.functions import add_then_multiply
from testing_lab.pipeline import PipelineStateful, PipelineStateless


def run_functions_pipeline() -> None:
    result = add_then_multiply(5)
    print(result)


def run_stateful_pipeline() -> None:
    p = PipelineStateful(5)
    p.run()
    result = p.x
    print(result)


def run_stateless_pipeline() -> None:
    p = PipelineStateless()
    result = p.run(5)
    print(result)


if __name__ == "__main__":
    run_functions_pipeline()
    run_stateful_pipeline()
    run_stateless_pipeline()
