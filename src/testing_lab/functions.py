def add(x: int) -> int:
    return x + 5


def multiply(x: int) -> int:
    return x * 3


def add_then_multiply(x: int) -> int:
    x = add(x)
    x = multiply(x)
    return x
