class PipelineStateful:
    def __init__(self, x: int) -> None:
        self.x = x

    def run(self):
        self.add()
        self.multiply()

    def add(self):
        self.x += 5

    def multiply(self):
        self.x *= 3


class PipelineStateless:
    def run(self, x: int) -> int:
        x = self.add(x)
        x = self.multiply(x)
        return x

    def add(self, x: int) -> int:
        return x + 5

    def multiply(self, x: int) -> int:
        return x * 3
