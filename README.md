# Testing lab

This repository contains a sample setup for testing your Python code.


## Testing principles

The reason we do automated testing is to be (more) certain that the code does what it's supposed to do. We distinguish two main approaches to testing: unittests and integration tests. **Unittesting** involves testing small code snippets in isolation, making sure that the building blocks for your program are sound. **Integration testing** involves testing large parts of the application at once, and are there to make sure these blocks are stacked together the right way. These two methods are both invaluable and should be used in conjunction.

### Unittests

For unittests in Python, the building blocks of your application are methods and functions. These are the snippets we want to unittest. But these snippets rarely operate in **isolation**; functions often call other functions. In order to satisfy the isolation requirement, we **mock** any calls to other local functions.

### Integration

Any test that breaks the isolation requirement can be considered an integration test. Usually though, integration tests test your whole application end-to-end. Mocking functions is not required.


## Writing testable code

Good code is testable code. Important things to keep in mind with regards to testing are:

* Short, single purpose functions (separation of concerns)
* Write stateless code (as opposed to stateful code)


## Tools

`pytest` adds some great functionality over the built-in `unittest`, like mocking environment variables. It's also way more readable. The framework will also run tests written in `unittest` or `nosetest`.
